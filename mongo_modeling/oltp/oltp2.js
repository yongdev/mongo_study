
/*

역으로 멤버 기준으로 카페를 리스트업 한다
*/
db.members.insertMany([
	{
		id: "tom93",
		first_name: "Tom",
		last_name: "Park",
		phone: "000-0000-1234",
		job: "DBA",
		joined_cafes: [
			{
				_id: 1,
				name: "IT Community",
				desc: "A Cafe where developer's share information.",
				created_at: ISODate("2018-08-09"),
				last_article: ISODate("2022-06-01T10:56:32.000Z"),
				level: 5,
				joined_at: ISODate("2018-09-12")
			},
			{
				_id: 2,
				name: "Game Community",
				desc: "Share information about games.",
				created_at: ISODate("2020-01-23"),
				last_article: ISODate("2022-06-02T10:56:32.000Z"),
				level: 4,
				joined_at: ISODate("2020-09-12")
			}
		]
	},
	{
		id: "asddwd12",
		first_name: "Jenny",
		last_name: "Kim",
		phone: "000-0000-1111",
		job: "Frontend Dev",
		joined_cafes: [
			{
				_id: 1,
				name: "IT Community",
				desc: "A Cafe where developer's share information.",
				created_at: ISODate("2018-08-09"),
				last_article: ISODate("2022-06-01T10:56:32.000Z"),
				level: 5,
				joined_at: ISODate("2018-10-02"),
			},
			{
				_id: 2,
				name: "Game Community",
				desc: "Share information about games.",
				created_at: ISODate("2020-01-23"),
				last_article: ISODate("2022-06-02T10:56:32.000Z"),
				level: 4,
				joined_at: ISODate("2021-10-01")
			}
		]
	},
	{
		id: "candy12",
		first_name: "Zen",
		last_name: "Ko",
		phone: "000-0000-1233",
		job: "DA",
		joined_cafes: [
			{
				_id: 1,
				name: "IT Community",
				desc: "A Cafe where developer's share information.",
				created_at: ISODate("2018-08-09"),
				last_article: ISODate("2022-06-01T10:56:32.000Z"),
				level: 5,
				joined_at: ISODate("2019-01-01")
			}
		]
	},
	{
		id: "java1",
		first_name: "Kevin",
		last_name: "Shin",
		phone: "000-0000-1133",
		job: "Game Dev",
		joined_cafes: [
			{
				_id: 2,
				name: "Game Community",
				desc: "Share information about games.",
				created_at: ISODate("2020-01-23"),
				last_article: ISODate("2022-06-02T10:56:32.000Z"),
				level: 4,
				joined_at: ISODate("2022-08-10")
			}
		]
	}
])

/*
  
 그러나 카페의 정보가 바뀐다면 전체적으로 유저마다 document를 수정해줘야함 
  
 */
arr = [];
for (i = 0; i < 300000; i++){
	document = {
		id: generateRandomString(5),
		first_name: generateRandomString(10),
		last_name: generateRandomString(15),
		phone: "000-0000-1234",
		job: jobs[Math.floor(Math.random() * jobs.length)],
		joined_cafes: [
			{
				_id: 2,
				name: 'Game Community',
				desc: 'Share information about games.',
				created_at: ISODate("2020-01-23T00:00:00.000Z"),
				last_article: ISODate("2022-06-02T10:56:32.000Z"),
				level: 4,
				joined_at: new Date(date - Math.floor(Math.random() * 10000000000)),
			}
		]
	}
	arr.push(document)
}



/*
1번 카페를 가입한 멤버가 많지 않아서 올래 걸리지 않지만
2번 카페를 가입한 사람이 많지 않아 오래걸린다
 */
date = new Date()
db.members.updateMany(
	{
		"joined_cafes._id": 1
	},
	{
		$set: {
			"joined_cafes.$.last_article": date
		}
	}
)

db.members.updateMany(
	{
		"joined_cafes._id": 2
	},
	{
		$set: {
			"joined_cafes.$.last_article": date
		}
	}
)

db.cafe.deleteMany({})
db.members.deleteMany({})