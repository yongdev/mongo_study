package com.example.spring_jpa_mongo.cmmy.service;

import com.example.spring_jpa_mongo.cmmy.dao.CmmyMongoRepository;
import com.example.spring_jpa_mongo.cmmy.dto.MyComment;
import com.example.spring_jpa_mongo.cmmy.dto.Summary;
import com.example.spring_jpa_mongo.cmmy.vo.Comment;
import com.example.spring_jpa_mongo.cmmy.vo.Post;
import com.example.spring_jpa_mongo.cmmy.vo.Recomment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class CmmyService {

    @Autowired
    private CmmyMongoRepository cmmyMongoRepository;

    public HashMap<String, Object> communityListMongoPost(HashMap<String, Object> params) throws Exception {

        HashMap<String, Object> result = new HashMap<>();
        List<Post> postList = cmmyMongoRepository.searchPostByMongo(params);
        result.put("postList", postList);
        return result;
    }

    public HashMap<String, Object> communityListMongoComment(HashMap<String, Object> params) throws Exception {


        HashMap<String, Object> result = new HashMap<>();
        List<Comment> commentList = cmmyMongoRepository.searchCommentByMongo(params);

        result.put("commentList", commentList);

        return result;
    }

    public HashMap<String, Object> communityListMongoJoinByRealList(HashMap<String, Object> params) throws Exception {

        HashMap<String, Object> result = new HashMap<>();
        List<Post> postList = CmmyMongoRepository.getPostListCommunityList(params);

        result.put("postList", postList);

        return result;
    }

    public HashMap<String, Object> getMyCommentListByUnion(HashMap<String, Object> params) throws Exception {


        HashMap<String, Object> result = new HashMap<>();
        List<MyComment> commentList = cmmyMongoRepository.getMyCommentListByUnion(params);

        result.put("commentList", commentList);

        return result;
    }

    public HashMap<String, Object> getMyCommentCntGroupBy(HashMap<String, Object> params) throws Exception {


        HashMap<String, Object> result = new HashMap<>();
        List<Summary> cntInfo = cmmyMongoRepository.getMyCommentCntGroupBy(params);

        result.put("cntInfo", cntInfo);

        return result;
    }

    public HashMap<String, Object> getPostUnwind(HashMap<String, Object> params) throws Exception {


        HashMap<String, Object> result = new HashMap<>();
        List<Post> cntInfo = cmmyMongoRepository.getPostUnwind(params);

        result.put("cntInfo", cntInfo);

        return result;
    }
}
