package com.example.spring_jpa_mongo.cmmy.vo;


import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;


@Document(collection = "post")
@Data
public class Post {

    @Id
    @Field("POST_NO")
    private int postNo;

    @Field("POST_USER")
    private String postUser;

    @Field("DISP_YN")
    private String dispYn;

    @Field("REG_DATE")
    private Date regDate;

    @Field("TITLE")
    private String title;

    @Field("SORT_SEQ")
    private int sortSeq;

    @Field("CMT_LIST")
    private List<Comment> commentList;

    @Field("POST_LIKE_LIST")
    private List<PostLike> postLikeList;

    @Field("POST_IMG_LIST")
    private List<PostImg> postImgList;


    @Field("LIKE_CNT")
    private int likeCnt;

    @Field("CMT_CNT")
    private int cmtCnt;

    @Field("DEPT_NO_1")
    private int depthNo1;

    @Field("DEPT_NO_2")
    private int depthNo2;
}
