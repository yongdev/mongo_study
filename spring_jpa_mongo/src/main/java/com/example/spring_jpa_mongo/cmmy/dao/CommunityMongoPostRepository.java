package com.example.spring_jpa_mongo.cmmy.dao;

import com.example.spring_jpa_mongo.cmmy.vo.Post;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CommunityMongoPostRepository extends MongoRepository<Post, Long> {
    public List<Post> findAllByDispYn(String disp_yn);
}
