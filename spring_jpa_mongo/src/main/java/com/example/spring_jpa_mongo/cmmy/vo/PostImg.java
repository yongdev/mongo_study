package com.example.spring_jpa_mongo.cmmy.vo;


import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Date;


@Document(collection = "post_img")
@Data
public class PostImg {

    @Field("IMG_PATH")
    private String imgPath;

    @Field("PRT_ID")
    private int prdId;

    @Field("SORT_SEQ")
    private int sortSeq;

    @Field("REG_DATE")
    private Date regDt;
}
