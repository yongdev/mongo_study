package com.example.spring_jpa_mongo.cmmy.dto;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class Summary {

    @Field("CMTY_TYPE")
    private String cmtyType;

    @Field("CMT_CNT")
    private int cmtCnt;

    @Field("USR_NO")
    private int usrNo;
}
