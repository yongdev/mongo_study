package com.example.spring_jpa_mongo.cmmy.vo;


import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;


@Document(collection = "post_like")
@Data
public class PostLike {


    @Field("USR_NO")
    private int usrNo;

    @Field("LIKE_YN")
    private String likeYn;

    @Field("PRT_ID")
    private int prdId;

    @Field("REG_DATE")
    private Date regDt;
}
