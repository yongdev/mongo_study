package com.example.spring_jpa_mongo.cmmy.dao;

import com.example.spring_jpa_mongo.cmmy.vo.Comment;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CommunityMongoCommentRepository extends MongoRepository<Comment, Long> {

}
