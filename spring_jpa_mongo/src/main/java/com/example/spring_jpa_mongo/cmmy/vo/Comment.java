package com.example.spring_jpa_mongo.cmmy.vo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;

@Document(collection = "comment")
@Data
public class Comment {

    @Id
    @Field("CMT_NO")
    private int commentNo;

    @Field("CMT")
    private String cmt;

    @Field("CMTY_TYPE")
    private String cmtyType;

    @Field("PRT_ID")
    private String prtId;

    @Field("USR_NO")
    private int usrNo;

    @Field("REG_DATE")
    private Date regDate;

    @Field("RECMT_LIST")
    private List<Recomment> recommentList;

    @Field("LIKE_CNT")
    private int likeCnt;

    @Field("CMT_CNT")
    private int cmtCnt;


}
