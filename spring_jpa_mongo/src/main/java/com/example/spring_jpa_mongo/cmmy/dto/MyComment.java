package com.example.spring_jpa_mongo.cmmy.dto;

import com.example.spring_jpa_mongo.cmmy.vo.Comment;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.Date;


@Data
public class MyComment {

    @Field("CMT_NO")
    private int cmtNo;

    @Field("RECMT_NO")
    private int recmtNo;

    @Field("CMT")
    private String cmt;

    @Field("CMTY_TYPE")
    private String cmtyType;

    @Field("PRT_ID")
    private String prtId;

    @Field("USR_NO")
    private int usrNo;

    @Field("REG_DATE")
    private Date regDate;
}
