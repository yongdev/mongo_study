package com.example.spring_jpa_mongo.cmmy.dao;


import com.example.spring_jpa_mongo.cmmy.dto.MyComment;
import com.example.spring_jpa_mongo.cmmy.dto.Summary;
import com.example.spring_jpa_mongo.cmmy.vo.Comment;
import com.example.spring_jpa_mongo.cmmy.vo.Post;
import com.example.spring_jpa_mongo.cmmy.vo.Recomment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
@Slf4j
public class CmmyMongoRepository {


    private static MongoTemplate mongoTemplate;

    public CmmyMongoRepository(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    @Autowired
    private CommunityMongoPostRepository communityMongoPostRepository;

    @Autowired
    private CommunityMongoCommentRepository communityMongoCommentRepository;

    public List<Post> searchPostByMongo(HashMap<String, Object> params) {
        List<Post> postList = communityMongoPostRepository.findAll();
        return postList;
    }

    public List<Comment> searchCommentByMongo(HashMap<String, Object> params) {
        List<Comment> commentList = communityMongoCommentRepository.findAll();
        return commentList;
    }

    public static List<Post> getPostListCommunityList(HashMap<String, Object> params) {

        /*
        * lookUp => RDMS LEFT JOIN
        * */
        LookupOperation lookUpComment = LookupOperation.newLookup()
                .from("comment")
                .localField("POST_NO")
                .foreignField("PRT_ID")
                .as("CMT_LIST");

        LookupOperation lookUpPostLike = LookupOperation.newLookup()
                .from("post_like")
                .localField("POST_NO")
                .foreignField("PRT_ID")
                .as("POST_LIKE_LIST");

        LookupOperation lookUpPostImg = LookupOperation.newLookup()
                .from("post_img")
                .localField("POST_NO")
                .foreignField("PRT_ID")
                .as("POST_IMG_LIST");

        /*
         * projection : 가져올 필드 명시
         * */
        ProjectionOperation projectionOperation  =
                Aggregation.project("POST_IMG_LIST")
                        .and("POST_NO").as("POST_NO")
                        .and("POST_USER").as("POST_USER")
                        .and("DISP_YN").as("DISP_YN")
                        .and("TITLE").as("TITLE")
                        .and("CMT_LIST").size().as("CMT_CNT")
                        .and("DEPT_NO_1").as("DEPT_NO_1")
                        .and("DEPT_NO_2").as("DEPT_NO_2")
                        .and("REG_DATE").as("REG_DATE")
                        .and("SORT_SEQ").as("SORT_SEQ")
//                        .and("CMT_LIST").as("CMT_LIST")
//                        .and("POST_IMG_LIST").as("POST_IMG_LIST")
                        .and("POST_LIKE_LIST").size().as("LIKE_CNT"); //컬럼 선택

        /*
         * match : RDMS where
         * */
        AggregationOperation matchOperation = Aggregation.match(Criteria
                .where("POST_NO").is(1)
//                .and("POST_IMG_LIST.SORT_SEQ").is(1)
        );


        /*
         * 페이지네이션 Operation
         * 아래의 2개를 조합해서 페이지네이션을 만든다
         * */
        SkipOperation skipOperation = Aggregation.skip(1); // 몇 개를 건너뛸지
        LimitOperation limitOperation = Aggregation.limit(1); // 몇 개만 가져올지

        //정렬 Operation
        SortOperation sortOperation = Aggregation.sort(Sort.Direction.ASC, "LIKE_CNT");

        //Group
        GroupOperation groupOperation = Aggregation.group("deptId", "deptName")
                .sum("sal").as("totalSal");

        /*
        * 자유도가 높은 만큼 Operation 순서가 성능에 영향을 미침
        * */
        Aggregation aggregation = Aggregation.newAggregation(
                matchOperation
                ,lookUpComment
                ,lookUpPostLike
                ,lookUpPostImg
                ,projectionOperation
                ,sortOperation
        );

        List<Post> postList = mongoTemplate.aggregate(aggregation, "post", Post.class).getMappedResults();


        return postList;
    }

    /*
    * union
    * */
    public static List<MyComment> getMyCommentListByUnion(HashMap<String, Object> params) {

        UnionWithOperation unionWithOperation = UnionWithOperation.unionWith("recomment");
        /*
         * projection : 가져올 필드 명시
         * */
        ProjectionOperation projectionOperation  =
                Aggregation.project()
                        .and("RECMT_NO").as("RECMT_NO")
                        .and("CMT_NO").as("CMT_NO")
                        .and("CMT").as("CMT")
                        .and("USR_NO").as("USR_NO")
                        .and("REG_DATE").as("REG_DATE")
                        .and("CMTY_TYPE").as("CMTY_TYPE")
                        .and("PRT_ID").as("PRT_ID");


        Aggregation aggregation = Aggregation.newAggregation(
                unionWithOperation
                ,projectionOperation
        );

        //return type은 따로 정해서 return 할 수 있다 => 이때 Projection 형식과 잘 맞출 것
        List<MyComment> myCommentList = mongoTemplate.aggregate(aggregation, "comment", MyComment.class).getMappedResults();
        log.info(String.valueOf(myCommentList.size()));
        return myCommentList;
    }

    public static List<Summary> getMyCommentCntGroupBy(HashMap<String, Object> params) {

        GroupOperation groupOperation = Aggregation.group("CMTY_TYPE","USR_NO")
                .count().as("CMT_CNT");

        /*
         * projection : 가져올 필드 명시
         * */
        ProjectionOperation projectionOperation  =
                Aggregation.project()
                        .and("CMTY_TYPE").as("CMTY_TYPE")
                        .and("USR_NO").as("USR_NO")
                        .and("CMT_CNT").as("CMT_CNT");


        Aggregation aggregation = Aggregation.newAggregation(
                groupOperation
                ,projectionOperation
        );

        List<Summary> myCommentList = mongoTemplate.aggregate(aggregation, "comment", Summary.class).getMappedResults();
        log.info("myCommentList  ==>   "  +myCommentList);
        log.info(String.valueOf(myCommentList.size()));
        return myCommentList;
    }

    public static List<Post> getPostUnwind(HashMap<String, Object> params) {

        LookupOperation lookUpComment = LookupOperation.newLookup()
                .from("comment")
                .localField("POST_NO")
                .foreignField("PRT_ID")
                .as("CMT_LIST");

        UnwindOperation unwindOperation = Aggregation.unwind("CMT_LIST");


        Aggregation aggregation = Aggregation.newAggregation(
                lookUpComment
//                ,unwindOperation
        );

        List<Post> postList = mongoTemplate.aggregate(aggregation, "post", Post.class).getMappedResults();

        return postList;
    }
}
