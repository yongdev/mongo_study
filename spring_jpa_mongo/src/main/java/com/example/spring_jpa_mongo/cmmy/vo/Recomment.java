package com.example.spring_jpa_mongo.cmmy.vo;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import javax.persistence.Id;
import java.util.Date;
import java.util.List;

@Document(collection = "recomment")
@Data
public class Recomment {

    @Id
    @Field("RECMT_NO")
    private int recommentNo;

    @Field("CMT_NO")
    private int commentNo;
    @Field("CMT")
    private String recmt;

    @Field("CMTY_TYPE")
    private String cmtyType;

    @Field("PRT_ID")
    private String prtId;

    @Field("USR_NO")
    private int usrNo;

    @Field("REG_DATE")
    private Date regDate;

}
