package com.example.spring_jpa_mongo.cmmy.controller;

import com.example.spring_jpa_mongo.cmmy.service.CmmyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.HashMap;


@RestController
@Slf4j
public class CmmyController {

    private final CmmyService cmmyService;

    public CmmyController(CmmyService cmmyService) {
        this.cmmyService = cmmyService;
    }

    @RequestMapping(value="/cmmy/post" , produces="application/json")
    @ResponseBody
    public HashMap<String, Object> communityListMongoPost(@RequestParam HashMap<String,Object> params, HttpSession session) throws Exception{
        return cmmyService.communityListMongoPost(params);
    }

    @RequestMapping(value="/cmmy/comment" , produces="application/json")
    @ResponseBody
    public HashMap<String, Object> communityListMongoComment(@RequestParam HashMap<String,Object> params, HttpSession session) throws Exception{
        return cmmyService.communityListMongoComment(params);
    }

    @RequestMapping(value="/cmmy/list" , produces="application/json")
    @ResponseBody
    public HashMap<String, Object> communityListMongoJoin(@RequestParam HashMap<String,Object> params) throws Exception{
//        return cmmyService.communityListMongoJoin(params);
        return null;
    }

    @RequestMapping(value="/cmmy/many/list" , produces="application/json")
    @ResponseBody
    public HashMap<String, Object> communityListMongoJoinByManyLookup(@RequestParam HashMap<String,Object> params) throws Exception{
//        return cmmyService.communityListMongoJoinByManyLookUp(params);
        return null;
    }

    @RequestMapping(value="/cmmy/real/list" , produces="application/json")
    @ResponseBody
    public HashMap<String, Object> communityListMongoJoinByRealList(@RequestParam HashMap<String,Object> params) throws Exception{
        return cmmyService.communityListMongoJoinByRealList(params);
    }

    @RequestMapping(value="/cmmy/union/list" , produces="application/json")
    @ResponseBody
    public HashMap<String, Object> commentListByUnion(@RequestParam HashMap<String,Object> params) throws Exception{
        return cmmyService.getMyCommentListByUnion(params);
    }

    @RequestMapping(value="/cmmy/group/info" , produces="application/json")
    @ResponseBody
    public HashMap<String, Object> getMyCommentCntGroupBy(@RequestParam HashMap<String,Object> params) throws Exception{
        return cmmyService.getMyCommentCntGroupBy(params);
    }

    @RequestMapping(value="/cmmy/unwind/postList" , produces="application/json")
    @ResponseBody
    public HashMap<String, Object> getPostUnwind(@RequestParam HashMap<String,Object> params) throws Exception{
        return cmmyService.getPostUnwind(params);
    }
}
