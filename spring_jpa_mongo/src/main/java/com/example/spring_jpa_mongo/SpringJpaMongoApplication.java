package com.example.spring_jpa_mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;



@SpringBootApplication
@ComponentScan(basePackages= {"com.example"} )
public class SpringJpaMongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringJpaMongoApplication.class, args);
    }

}
