db.restaurants.find(
    {borough:"Brooklyn"}
).explain('executionStats')

/*
 * 
 *     nReturned: 6086, 반환된 doc 수
    executionTimeMillis: 16, 쿼리 수행 시간
    totalKeysExamined: 0, index를 얼마나 읽엇는지
    totalDocsExamined: 25359, 결과 반환까지 조회한 doc 수


 */


db.restaurants.find(
    {borough:"Brooklyn"},
    {name:1, borough:1}
).sort({name:1}).explain('executionStats')
    

/**
 *     executionStages: {
      stage: 'PROJECTION_SIMPLE',
      nReturned: 6086,
      executionTimeMillisEstimate: 2,
      works: 25361,
      advanced: 6086,
      needTime: 19274,
      needYield: 0,
      saveState: 25,
      restoreState: 25,
      isEOF: 1,
      transformBy: { name: 1, borough: 1 },
      inputStage: {  // 안쪽부터 실행됨
        stage: 'COLLSCAN',
        filter: { borough: { '$eq': 'Brooklyn' } },
        nReturned: 6086,
        executionTimeMillisEstimate: 2,
        works: 25361, //얼마나 많은 작업 단위를 하는지
        advanced: 6086, //부모 stage로 넘긴 값들
        needTime: 19274,
        needYield: 0,
        saveState: 25,
        restoreState: 25,
        isEOF: 1,
        direction: 'forward',
        docsExamined: 25359
      }

 * 
 * 
 */


db.restaurants.createIndex({borough : 1}) //인덱스 생성

db.restaurants.createIndex({name: 1 , borough : -1}) 

db.restaurants.find(
    {borough:"Brooklyn"},
    {name:1, borough:1}
).sort({name:1}).explain('allPlansExecution')

db.restaurants.aggregate([
    {
        $match: {borough: "Brooklyn"}
    },
    {
        $group: {
            _id: "$cuisine",
            cnt: {$sum: 1}
        }
    },
    {
        $sort: {
            name: 1
        }
    }
]).explain('executionStats')