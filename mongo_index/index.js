//실행계획까지 볼 수 있음
db.zips.find(
    {
        state: "LA",
        pop : {
            $gte : 40000
        }
    }
).sort({city:1}).explain('executionStats') 


 // 1 오름차순, -1 내림차순
db.zips.createIndex({state:1})


db.zips.createIndex({state:1, city:1, pop:1})


/* 
어떤 projection을 인덱스로 지정하면
인덱스에 생성된 데이터로 보여주기 때문에 
totalDocsExamined 0개 조회함
*/

db.zips.find(
    {
        state: "LA",
        pop : {
            $gte : 40000
        }
    },{
        _id:0,
        state:1,
        pop : 1,
        city:1
    }
).sort({city:1}).explain('executionStats') 
