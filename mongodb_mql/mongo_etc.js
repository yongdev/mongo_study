db.bulk.bulkWrite([
        {insertOne : {doc:1, order : 11}},
        {insertOne : {doc:2, order : 12}},
        {insertOne : {doc:3, order : 13}},
        {insertOne : {doc:4, order : 14}},
        {insertOne : {doc:5, order : 15}},
        {insertOne : {doc:6, order : 16}},
        {insertOne : {doc:7, order : 17}},
        {insertOne : {doc:8, order : 18}},
        {
            deleteOne : {
                filter : {doc:3}
            }
        },
        {
            updateOne : {
                filter : {doc:2},
                update : {
                    $set : {doc:12}
                }
            }
        }    
    ],
    {ordered: false} //false일경우 순서와 상관없이 최적화되게 실행 
)

//mongodb는 document 단위로 무결성 보장함
//mongodb는 transactions을 지원하지만 권장 x

db.bulk.countDocuments()

db.bulk.distinct("doc")

db.bulk.findAndModify({
    query : {doc :4},
    update : {$inc : {doc : 1}}
})


db.bulk.findAndModify({
    query : {doc :5},
    sort : {order:-1}, //내림차순
    update : {$inc : {doc : 1}}
})

db.bulk.getIndexes()
db.bulk.createIndex({doc:1}) //오름차순 인덱스 생성

//mongo db는 _id는 수정되지 않는다 
db.bulk.updateOne(
    {doc:1},
    {
        $set : {
            order :100
        }
    }        
)

db.bulk.replaceOne(
    {
        doc:1
    },
    {
        doc:11,
        order: 200
    }
)