//내장 document 접근

db.sales.findOne({
    "customer.email":"keecade@hem.uy"
}) //이렇게 안걸면 전체를 다적어줘야 조회할 수 있다.


db.inventory.insertMany([
    {item: "j1" , qty : 25, tages:["blank1", "reddfgdf"], dim_cm : [22, 30]},
    {item: "j2" , qty : 26, tages:["blank2", "reddfg"], dim_cm : [23, 30]},
    {item: "j3" , qty : 27, tages:["blank3", "reddfg"], dim_cm : [24, 30]},
    {item: "j4" , qty : 28, tages:["blank4", "redsdfg"], dim_cm : [25, 32]},
    {item: "j5" , qty : 29, tages:["blank5", "red523"], dim_cm : [11, 3345340]},
    {item: "j6" , qty : 30, tages:["blank6", "red6"], dim_cm : [22, 31]},
])

db.inventory.find({
    tages : ["blank3"]
})

//and
db.inventory.find({
    tages : { $all : ["blank3" , "reddfg"]}
})

//or
db.inventory.find({
    tages : { $in : ["blank3" , "reddfg"]}
})

db.inventory.find({
    qty : { $gt :27}
})

db.inventory.find({
    dim_cm : { $gt :27, $lt : 1000} //얘는 요소라도 조건을 만족해야함
})

//요소들이 모두 만족해야함 elemMatch 조건이 맞아야 나옴
db.inventory.find({
    dim_cm : {$elemMatch : { $gt :27, $lt : 1000}} 
})

//사이즈 확인
db.inventory.find({
   tages : {$size:2}
})


db.sales.find({
    items: {
        $elemMatch : {
            name:"binder",
            quantity: {$lte:6}
        }
    }
 })

 db.sales.find({
    "items.name" : "binder",
    "items.quantity" : {$lte : 6}
 })

 /////////

 db.students.insertMany([
    {_id:1, grades:[85,80,80]},
    {_id:2, grades:[85,99,95]},
    {_id:3, grades:[85,70,21]}
 ])

 //첫 번째 만나느 값이 바낌
 db.students.updateOne(
    {_id:1, grades : 80},
    {$set: {"grades.$" : 85}}
 )

 //전체 데이터 값 변경
 db.students.updateMany(
    {},
    {$inc: {"grades.$[]" : 3}} // $[] 배열 전체를 바꿔주겠다
 )

//
 db.students.insertMany([
    {_id:4, 
        grades: [
            {grade:80, mean:75, std:8},
            {grade:85, mean:90, std:5},
            {grade:85, mean:85, std:8}
        ]
    }
 ])
 
//위의 데이터 수정
 db.students.updateOne(
    {_id:4, "grades.grade" : 85},
    {$set: {"grades.$.std" : 6}}
 )

 //
 db.students.insertMany([
    {_id:6, 
        grades: [
            {grade:90, mean:75, std:8},
            {grade:87, mean:90, std:6},
            {grade:85, mean:85, std:8}
        ]
    }
 ])


 db.students.updateMany(
    {_id:6},
    {$set: {"grades.$[element].grade": 100}}, //element가 {} 객체 하나라고 생각하면 됨 => 변수할당 개념
    {arrayFilters : [{"element.grade" : {$gte:87}}]}
 )


 db.students.insertMany([
    {_id:7, 
        grades: [
            {type: "quiz", questions:[10,8,5]},
            {type: "quiz", questions:[8,9,6]},
            {type: "hw", questions:[5,4,3]},
            {type: "exam", questions:[25,10,23, 0]}
        ]
    }
 ])

 db.students.updateOne( //One일때 대괄호 안들어감
    {_id:7},
    {$inc : {"grades.$[].questions.$[score]": 2}}, //score 에 각 요소들 할당, 그리고 필터로 조건 맞춘거만 수정
    {arrayFilters : [{score : {$gte : 8}}]}
 )


 db.shopping.insertMany([
    {_id:1, cart : ["banana", "cheeze", "milk"], coupons : ["10%", "20%", "30%"]},
    {_id:2, cart : [], coupons : []}
 ])

 //set 구조로 들어감
 db.shopping.updateOne(
    {_id : 1},
    {$addToSet : {cart:"beer"}}
 )

 //이러면 배열 자체가 들어감
 db.shopping.updateOne(
    {_id : 1},
    {$addToSet : {cart:["beer", "candy"]}}
 )

 //이렇게 해야 여러개를 set 고려하여 들어감
 db.shopping.updateOne(
    {_id : 1},
    {$addToSet : {cart:{$each : ["beer", "candy"]}}}
 )

 //삭제
 db.shopping.updateOne(
    {_id : 1},
    {$pull : {cart:"beer"}}
 )

  //여러개 삭제
  db.shopping.updateOne(
    {_id : 1},
    {$pull : {cart: {$in : [['beer','candy'], 'milk'] } }}
 )

 db.shopping.updateOne(
    {_id : 1},
    {$pop : {cart:-1}} //배열의 첫번째 값 삭제
 )

 db.shopping.updateOne(
    {_id : 1},
    {$pop : {cart:1, coupons : -1}} //배열의 뒤에서 첫번째 값, 배열의 첫번째 값
 )


 db.shopping.updateOne(
    {_id : 1},
    {$push : {cart:"good"}}
 )

 db.shopping.updateOne(
    {_id : 1},
    {$push : {coupons:{$each : ["50%", "60%"]}}}
 )

//특정 포지션에다가 push
 db.shopping.updateOne(
    {},
    {
        $push : {
            coupons:{
                $each : ["90%", "70%"],
                $position :0
            }
        }
    }
 )

 
 db.shopping.updateMany(
    {},
    {
        $push : {
            coupons:{
                $each : ["90%", "70%"],
                $position :0
            }
        }
    }
 )

 //슬라이싱도 할 수 있다
 db.shopping.updateMany(
    {},
    {
        $push : {
            coupons:{
                $each : ["10%", "20%"],
                $position :0,
                $slice : 5
            }
        }
    }
 )

 //배열 단위로 정렬도 가능
 db.shopping.updateMany(
    {},
    {
        $push : {
            coupons:{
                $each : ["99%", "98%"],
                $position :-1,
                $sort : -1, //내림차순
                $slice : 5
            }
        }
    }
 )