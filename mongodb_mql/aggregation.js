db.order.insertMany([
    {_id:0, name:"yongwon0", size:"s" , price : 19, quantity:13, date :ISODate("2021-03-13T08:14:30Z")},
    {_id:1, name:"yongwon1", size:"m" , price : 20, quantity:14, date :ISODate("2021-03-14T08:14:30Z")},
    {_id:2, name:"yongwon2", size:"l" , price : 21, quantity:15, date :ISODate("2021-03-15T08:14:30Z")},
    {_id:3, name:"yongwon3", size:"s" , price : 22, quantity:16, date :ISODate("2021-03-16T08:14:30Z")},
    {_id:4, name:"yongwon4", size:"m" , price : 23, quantity:17, date :ISODate("2022-01-13T08:14:30Z")},
    {_id:5, name:"yongwon5", size:"l" , price : 24, quantity:18, date :ISODate("2022-02-13T08:14:30Z")},
    {_id:6, name:"yongwon6", size:"s" , price : 25, quantity:19, date :ISODate("2022-03-13T08:14:30Z")},
    {_id:7, name:"yongwon7", size:"m" , price : 26, quantity:20, date :ISODate("2022-04-13T08:14:30Z")}
])


//이름에 따라서 quantity가 합이 얼마나 통계
db.order.aggregate([
    {$match : {size:"m"}},
    {
        $group : {
            _id : "$name", 
            totalQuantity : {
                $sum: "$quantity"
            }
        }
    }
])

db.order.aggregate([
    {$match: {date : {
        $gte : new ISODate("2020-01-30"),
        $lt : new ISODate("2022-01-30"),
    }}},
    {
        $group : {
            _id : {
                $dateToString : {
                    format : "%Y-%m-%d", date : "$date"
                }
            },
            totalOrderValue: { //sql 에서 alias 주는거랑 똑같은듯
                $sum : {
                    $multiply : ["$price", "$quantity"]
                }
            },
            averageOrderQuantity : {
                $avg : "$quantity"
            }
        }
    },
    {
        $sort : {
            averageOrderQuantity : -1
        }
    }
])

db.books.insertMany([    
    {"_id":8751, "title":"The Banquet1", "author": "Dante1", "copies": 10},{"_id":8754, "title":"The Banquet4", "author": "Dante2", "copies": 12},
    {"_id":8752, "title":"The Banquet2", "author": "Dante1", "copies": 11},{"_id":8755, "title":"The Banquet5", "author": "Dante3", "copies": 13},
    {"_id":8753, "title":"The Banquet3", "author": "Dante2", "copies": 15},{"_id":8756, "title":"The Banquet6", "author": "Dante3", "copies": 14},
])

//group by 한 결과를 books라는 배열에 각각 담아서 표현해라
db.books.aggregate([
    {$group : {
        _id : "$author",
        books : {
            $push: "$title"
        }
    }}
])

//$$ROOT는 시스템 변수인데 요약정보를 담을 때 다 담아서 보여준다
db.books.aggregate([
    {$group : {
        _id : "$author",
        books : {
            $push: "$$ROOT"
        }
    }}
])

//각각 저자마다 그룹by 정보에 totalCopies라는 가공정보를 넣는다
db.books.aggregate([
    {$group : {_id : "$author", books : {$push:"$$ROOT"}}},
    {
        $addFields:{
            totalCopies : {$sum : "$books.copies"}
        }
    }
])

//lookup ==> sql의 join과 같음

db.orders.insertMany([    
    {"productId":1, "price":12},
    {"productId":2, "price":13},
    {"productId":3, "price":14},
])

db.products.insertMany([    
    {"id":1, "instock":120},
    {"id":2, "instock":80},
    {"id":3, "instock":60},
    {"id":4, "instock":70},
])

db.orders.aggregate([
    {
        $lookup:{
            from : 'products',
            localField : 'productId',
            foreignField : "id",
            as: 'data'  //join 된 결과로 나옴
        }
    }
])


db.books.aggregate([
    {$group : {_id : "$author", books : {$push:"$$ROOT"}}},
    {
        $addFields:{
            totalCopies : {$sum : "$books.copies"}
        }
    },
    {
        $unwind : "$books" //배열[객체형태를 다풀어서  각각 결과를 내줌] 
    }
])

//에어비앤비 데이터 
db.listingsAndReviews.aggregate([
    {
        $sample : {size : 3} //랜덤으로
    },{
        $project : {
            name:1,
            summary:1
        }
    }
])

db.listingsAndReviews.aggregate([
    {
        $match : {
            property_type : "Apartment"
        } //랜덤으로
    },
    {
        $sort : {
            number_of_reviews: -1
        }
    },
    {
        $skip : 2 //페이지
    },
    {
        $limit : 5 //5개씩 보여줌
    },
    {
        $project : { //보여줄 필드만
            name:1,
            number_of_reviews : 1 
        }
    }
])

//결과를 컬렉션에 넣어서 저장
db.books.aggregate([
    {
            $group : {
            _id : "$author",
            books : {$push : "$title"}
        }
    },
    {
        $out : "authors" //해당 컬렉션에 결과를 저장하겠다
    }
])