use stores

db.restaurants.insertMany( [
    {
       _id: 1,
       name: "American Steak House",
       food: [ "filet", "sirloin" ],
       beverages: [ "beer", "wine" ]
    },
    {
       _id: 2,
       name: "Honest John Pizza",
       food: [ "cheese pizza", "pepperoni pizza" ],
       beverages: [ "soda" ]
    }
 ] )

 db.orders.insertMany( [
    {
       _id: 1,
       item: "filet",
       restaurant_name: "American Steak House"
    },
    {
       _id: 2,
       item: "cheese pizza",
       restaurant_name: "Honest John Pizza",
       drink: "lemonade"
    },
    {
       _id: 3,
       item: "cheese pizza",
       restaurant_name: "Honest John Pizza",
       drink: "soda"
    }
 ] )

//join할 테이블이고
//left outer join이며,
//$unwind는 rdms join 처럼 각각으로 풀어준다 => 아니면 배열 형태로 한번에 담김
 db.restaurants.aggregate([
    {
        $lookup: { 
            from: "orders",
            localField: "name",
            foreignField: "restaurant_name",
            as: "orders"
        }
    },{
        $unwind : "$orders"
    }
 ])

