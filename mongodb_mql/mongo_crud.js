//단일 추가
db.employees.insertOne({
    name:"lake",
    age:21,
    dept : "DataBase",
    joinDate: new ISODate("2022-10-01"),
    salary : 40000,
    bonus : null
})


db.employees.insertMany([
    {  
        name:"king",
        age:21,
        dept : "DataBase",
        joinDate: new ISODate("2022-10-01"),
        salary : 40000,
        bonus : null
    },
    {
        name:"tomy",
        age:25,
        dept : "java",
        joinDate: new ISODate("2022-11-01"),
        salary : 400040,
        bonus : 1000,
        isNegotiating : true
    }
    ] 
)

// 2개이상이면 many가 빠르다
// mongo db는 js 문법이 먹는다
let docs = [];
for (i=0; i < 300; i++) {
    docs.push({a:1})
}
db.insertTest.insertMany(docs);


//업데이트
db.employees.updateOne(
    {name:"tomy"}, //key로 조회  ==> 쿼리필터
    { //오퍼레이터 구간
        $set:{
          salary:35000,
          dept : "deveops",
          joinDate : new ISODate("2022-12-31")
        },
        $unset : {
            isNegotiating :""
        }
    }
)

//다중업데이트
//쿼리 필터 존재여부 추가
db.employees.updateMany(
    {resignationDate : { $exists : false }, joinDate : { $exists : true}},
    {$mul : {salary:Decimal128("1.1")}} //$mul 곱하기 약자
)

db.employees.updateMany(
    {name:"tomy"},
    {
        $unset : {bonus :null}
    }
)

db.employees.updateMany(
    {resignationDate : { $exists : false }, joinDate : { $exists : true}},
    {$set : {bonus:170000}} // 이렇게 되는 경우 보너스가 있던 애들도 업데이트 된다
)

db.employees.updateMany(
    {resignationDate : { $exists : false }, bonus : { $exists : true}},
    {$set : {bonus:340000}} 
)

db.employees.deleteOne({name : "tomy"})
db.employees.deleteMany({})

db.planets.findOne({name:"Mars"})
db.planets.find({hasRings:true , orderFromSun : {$lte:6}}) //an d
//and 조건
db.planets.find(
        {
            $and : [
                {hasRings:true},
                {orderFromSun : {$lte : 6}}
            ]
        }
    )

db.planets.find(
    {
        $or : [
            {hasRings: {$ne : false}},
            {orderFromSun : {$gt : 6}}
        ]
    }
)    

//리스트 형태일때 포함여부도 찾는 거 가능하다
db.planets.find(
    {mainAtmosphere: {$in:['O2']}}
)
